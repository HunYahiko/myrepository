#include "Deck.h"

size_t Deck::m_index = 0;

Deck::Deck()
{
	for (size_t number = static_cast<size_t>(Number::ONE); number <= static_cast<size_t>(Number::THREE); number++)
	{
		for (size_t symbol = static_cast<size_t>(Symbol::DIAMOND); symbol <= static_cast<size_t>(Symbol::OVAL); symbol++)
		{
			for (size_t shading = static_cast<size_t>(Shading::SOLID); shading <= static_cast<size_t>(Shading::OPEN); shading++)
			{
				for (size_t color = static_cast<size_t>(Color::RED); color <= static_cast<size_t>(Color::BLUE); color++)
				{
					m_deck.push_back(
						Card(
							static_cast<Number>(number),
							static_cast<Symbol>(symbol),
							static_cast<Shading>(shading),
							static_cast<Color>(color))
					);
				}
			}
		}
	}
}


Deck::~Deck()
{
}

std::ostream& operator <<(std::ostream &out, const Deck &deck) 
{
	for (auto const &card : deck.m_deck)
	{
		out << card << std::endl;
	}

	return out;
}

Card Deck::getACard()
{
	return m_deck[m_index++];
}

void Deck::swap(Card &firstCard, Card &secondCard)
{
	Card temp = firstCard;
	secondCard = firstCard;
	secondCard = temp;
}

void Deck::shuffle()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, deckSize - 1);
	for (auto i = 0; i < deckSize; ++i)
	{
		swap(m_deck.at(i), m_deck.at(dis(gen)));
	}
}
