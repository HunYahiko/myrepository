#include "Card.h"



Card::Card()
{
}

Card::Card(const Number &number, const Symbol &symbol, const Shading &shading, const Color &color)
	: m_number(number),
	m_symbol(symbol),
	m_shading(shading),
	m_color(color)
{
}

Card::Card(const Card& card)
{
	m_color = card.m_color;
	m_shading = card.m_shading;
	m_symbol = card.m_symbol;
	m_number = card.m_number;
}

Card::~Card()
{
}

std::ostream& operator <<(std::ostream &out, const Card &card)
{
	out << card.toString(card.m_number) << " "
		<< card.toString(card.m_symbol) << " "
		<< card.toString(card.m_shading) << " "
		<< card.toString(card.m_color) << std::endl;

	return out;
}

Card& Card::operator =(const Card &card)
{
	m_number = card.m_number;
	m_symbol = card.m_symbol;
	m_shading = card.m_shading;
	m_color = card.m_color;

	return *this;
}

Number Card::getNumber() const
{
	return m_number;
}

Symbol Card::getSymbol() const
{
	return m_symbol;
}

Shading Card::getShading() const
{
	return m_shading;
}

Color Card::getColor() const
{
	return m_color;
}



std::string Card::toString(const Number &number) const
{
	switch (number)
	{
	case Number::ONE:
		return "One";
		break;

	case Number::TWO:
		return "Two";
		break;

	case Number::THREE:
		return "Three";
		break;
	}
}

std::string Card::toString(const Symbol &symbol) const
{
	switch (symbol)
	{
	case Symbol::DIAMOND:
		return "Diamond";
		break;

	case Symbol::OVAL:
		return "Oval";
		break;

	case Symbol::SQUIGGLE:
		return "Squiggle";
		break;
	}
}

std::string Card::toString(const Shading &shading) const
{
	switch (shading)
	{
	case Shading::OPEN:
		return "Open";
		break;

	case Shading::SOLID:
		return "Solid";
		break;

	case Shading::STRIPED:
		return "Stripped";
		break;
	}
}

std::string Card::toString(const Color &color) const
{
	switch (color)
	{
	case Color::BLUE:
		return "Blue";
		break;

	case Color::GREEN:
		return "Green";
		break;

	case Color::RED:
		return "Red";
		break;
	}
}
