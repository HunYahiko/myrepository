#pragma once
#include "Deck.h"
class IDealer
{
public:

	virtual Card dealCard(Deck &deck) const = 0;
	virtual void shuffleDeck(Deck &deck) const = 0;

	IDealer();
	~IDealer();
};

