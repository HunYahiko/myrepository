#pragma once
#include "Card.h"
#include <array>
class Set
{
private:
	static const size_t oneSet = 3;
	std::array<Card, oneSet> m_set;
public:

	Set(const Card &firstCard, const Card &secondCard, const Card &thirdCard);
	~Set();

	bool validateSet();

	bool areEqual(const Number &firstNumber, const Number &secondNumber, const Number &thirdNumber) const;
	bool areEqual(const Symbol &firstSymbol, const Symbol &secondSymbol, const Symbol &thirdSymbol) const;
	bool areEqual(const Shading &firstShading, const Shading &secondShading, const Shading &thirdShading) const;
	bool areEqual(const Color &firstColor, const Color &secondColor, const Color &thirdColor) const;

    bool areDifferent(const Number &firstNumber, const Number &secondNumber, const Number &thirdNumber) const;
	bool areDifferent(const Symbol &firstSymbol, const Symbol &secondSymbol, const Symbol &thirdSymbol) const;
	bool areDifferent(const Shading &firstShading, const Shading &secondShading, const Shading &thirdShading) const;
	bool areDifferent(const Color &firstColor, const Color &secondColor, const Color &thirdColor) const;
};

