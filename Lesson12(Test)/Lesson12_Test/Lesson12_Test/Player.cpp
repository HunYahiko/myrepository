#include "Player.h"

size_t Player::m_index = 0;

Player::Player() : Player("Dealer")
{

}

Player::Player(std::string name)
	: m_name(name)
{
	m_index++;
}


Player::~Player()
{
}

void Player::shuffleDeck(Deck &deck) const
{
	deck.shuffle();
}

Card Player::dealCard(Deck &deck) const
{
	return deck.getACard();
}




