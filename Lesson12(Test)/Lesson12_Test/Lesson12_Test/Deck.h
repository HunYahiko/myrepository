#pragma once
#include "Card.h"
#include <vector>
#include <random>

class Deck
{
private:
	static const size_t deckSize = 81;
	static size_t m_index;
	std::vector<Card> m_deck;

public:
	Deck();
	~Deck();

	friend std::ostream& operator <<(std::ostream &out, const Deck &deck);

	Card getACard();

	void shuffle();

	void swap(Card &firstCard, Card &secondCard);
};

