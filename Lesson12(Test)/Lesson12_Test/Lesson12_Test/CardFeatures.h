#pragma once

enum Number
{
	ONE,
	TWO,
	THREE
};

enum Symbol
{
	DIAMOND,
	SQUIGGLE,
	OVAL
};

enum Shading
{
	SOLID,
	STRIPED,
	OPEN
};

enum Color
{
	RED,
	GREEN,
	BLUE
};
