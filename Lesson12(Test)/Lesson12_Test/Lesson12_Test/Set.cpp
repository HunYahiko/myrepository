#include "Set.h"



Set::Set(const Card &firstCard, const Card &secondCard, const Card &thirdCard)
{
	m_set[0] = firstCard;
	m_set[1] = secondCard;
	m_set[2] = thirdCard;
}


Set::~Set()
{
}

bool Set::validateSet()
{
	if (!areEqual(m_set[0].getNumber(), m_set[1].getNumber(), m_set[2].getNumber()) && !areDifferent(m_set[0].getNumber(), m_set[1].getNumber(), m_set[2].getNumber()))
	{
		return false;
	}

	if (!areEqual(m_set[0].getSymbol(), m_set[1].getSymbol(), m_set[2].getSymbol()) && !areDifferent(m_set[0].getSymbol(), m_set[1].getSymbol(), m_set[2].getSymbol()))
	{
		return false;
	}

	if (!areEqual(m_set[0].getShading(), m_set[1].getShading(), m_set[2].getShading()) && !areDifferent(m_set[0].getShading(), m_set[1].getShading(), m_set[2].getShading()))
	{
		return false;
	}

	if (!areEqual(m_set[0].getColor(), m_set[1].getColor(), m_set[2].getColor()) && !areDifferent(m_set[0].getColor(), m_set[1].getColor(), m_set[2].getColor()))
	{
		return false;
	}

	return true;
}


bool Set::areEqual(const Number &firstNumber, const Number &secondNumber, const Number &thirdNumber) const
{
	return firstNumber == secondNumber == thirdNumber;
}

bool Set::areEqual(const Symbol &firstSymbol, const Symbol &secondSymbol, const Symbol &thirdSymbol) const
{
	return firstSymbol == secondSymbol == thirdSymbol;
}

bool Set::areEqual(const Shading &firstShading, const Shading &secondShading, const Shading &thirdShading) const
{
	return firstShading == secondShading == thirdShading;
}

bool Set::areEqual(const Color &firstColor, const Color &secondColor, const Color &thirdColor) const
{
	return firstColor == secondColor == thirdColor;
}




bool Set::areDifferent(const Number &firstNumber, const Number &secondNumber, const Number &thirdNumber) const
{
	return firstNumber != secondNumber != thirdNumber;
}

bool Set::areDifferent(const Symbol &firstSymbol, const Symbol &secondSymbol, const Symbol &thirdSymbol) const
{
	return firstSymbol != secondSymbol != thirdSymbol;
}

bool Set::areDifferent(const Shading &firstShading, const Shading &secondShading, const Shading &thirdShading) const
{
	return firstShading != secondShading != thirdShading;
}

bool Set::areDifferent(const Color &firstColor, const Color &secondColor, const Color &thirdColor) const
{
	return firstColor != secondColor != thirdColor;
}
