#pragma once
#include "IDealer.h"
#include "Set.h"

class Player
	: public IDealer
{
private:
	static size_t m_index;
	std::vector<Set> m_sets;
	size_t points = 0;
	std::string m_name;

public:
	Player();
	Player(std::string name);
	~Player();

	void shuffleDeck(Deck &deck) const override;
	Card dealCard(Deck &deck) const override;
};

