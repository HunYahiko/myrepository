#pragma once
#include "CardFeatures.h"
#include <iostream>
#include <string>

class Card
{
private:
	Number m_number;
	Symbol m_symbol;
	Shading m_shading;
	Color m_color;

	std::string toString(const Number &number) const;
	std::string toString(const Symbol &symbol) const;
	std::string toString(const Shading &shading) const;
	std::string toString(const Color &color) const;

public:
	Card();
	Card(const Number &number, const Symbol &symbol, const Shading &shading, const Color &color);
	Card(const Card& card);
	~Card();

	Card& operator =(const Card &card);

	Number getNumber() const;
	Symbol getSymbol() const;
	Shading getShading() const;
	Color getColor() const;
	

	friend std::ostream& operator <<(std::ostream &out, const Card &card);
};

