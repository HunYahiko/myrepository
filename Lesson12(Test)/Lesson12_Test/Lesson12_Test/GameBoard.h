#pragma once
#include "Deck.h"
#include "Player.h"

class GameBoard
{
private:
	static const size_t initialNumberOfCards = 12;
	std::vector<Card> m_cards;
	std::vector<Player> m_players;
	Player m_dealer;
	Deck deck;

public:
	GameBoard();
	~GameBoard();

	Set findSet();
};

