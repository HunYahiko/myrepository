
#include "BMPTransformationHeader.h"

int main() {

	uint8_t *label = nullptr;

	Image *images = nullptr;

	uint32_t numberOfImages = 0;

	label = getLabels();

	images = getImages(numberOfImages);

	createBMPImage(images, label, numberOfImages);
	
	return 0;
}

