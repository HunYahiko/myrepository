#include "BMPTransformationHeader.h"

int smallEndianConverter(uint32_t number) {

	unsigned char first, second, third, fourth;

	first = number & 255;

	second = (number >> 8) & 255;

	third = (number >> 16) & 255;

	fourth = (number >> 24) & 255;

	return ((int)first << 24) + ((int)second << 16) + ((int)third << 8) + fourth;
}

void flipImageArray(uint8_t *pixels, int numberOfRows, int numberOfColumns) {

	for (int i = 0; i < numberOfRows / 2; i++) {

		for (int j = 0; j < numberOfColumns; j++) {

			uint32_t index1 = i * numberOfRows + j;

			uint32_t index2 = (numberOfRows - i - 1) * numberOfColumns + j;

			std::swap(pixels[index1], pixels[index2]);
		}
	}
}

void checkIfFileIsOpen(std::ifstream &file, std::string fileName) {
	if (!file.is_open()) {

		std::cout << "Could not open " << fileName << " file!" << std::endl;
		exit(0);
	}
}

uint8_t * getLabels() {
	std::ifstream labelsFile(labelsFileName, std::ios::binary);

	checkIfFileIsOpen(labelsFile, labelsFileName);

	uint32_t magicNumber, numberOfLabels;

	labelsFile.read((char*)&magicNumber, sizeof(uint32_t));
	magicNumber = smallEndianConverter(magicNumber);

	labelsFile.read((char*)&numberOfLabels, sizeof(uint32_t));
	numberOfLabels = smallEndianConverter(numberOfLabels);

	uint8_t *labels = new uint8_t[numberOfLabels];

	const size_t labelSize = numberOfLabels * sizeof(labels[0]);

	labelsFile.read((char *)labels, labelSize);

	labelsFile.close();

	return labels;
}


Image * getImages(uint32_t &numberOfImages) {
	std::ifstream imagesFile(imagesFileName, std::ios::binary);

	checkIfFileIsOpen(imagesFile, imagesFileName);

	uint32_t magicNumber, numberOfRows , numberOfColumns;
	

	imagesFile.read((char*)&magicNumber, sizeof(uint32_t));
	magicNumber = smallEndianConverter(magicNumber);

	imagesFile.read((char*)&numberOfImages, sizeof(uint32_t));
	numberOfImages = smallEndianConverter(numberOfImages);

	imagesFile.read((char*)&numberOfRows, sizeof(uint32_t));
	numberOfRows = smallEndianConverter(numberOfRows);

	imagesFile.read((char*)&numberOfColumns, sizeof(uint32_t));
	numberOfColumns = smallEndianConverter(numberOfColumns);

	Image *images = new Image[numberOfImages];

	for (int i = 0; i < numberOfImages; i++) {

		images[i].pixels = new uint8_t[numberOfColumns * numberOfRows];

		size_t imageSize = sizeof(images[i].pixels[0]) * numberOfRows * numberOfColumns;	// in bytes

		imagesFile.read((char*)images[i].pixels, imageSize);
	}
	images->columns = numberOfColumns;

	images->rows = numberOfRows;

	return images;
}

void createBMPheaders(HEADER &header, INFOHEADER &infoheader, Image *images) {
	memset(&header, 0, headerSize);

	memset(&infoheader, 0, infoheaderSize);

	const size_t imageSize = sizeof(images[0].pixels[0]) * images[0].rows * images[0].columns;

	infoheader.size = infoheaderSize;

	infoheader.width = images[0].columns;

	infoheader.height = images[0].rows;

	infoheader.planes = 1;

	infoheader.bits = 8;

	infoheader.compression = 0;

	infoheader.imagesize = imageSize;

	infoheader.ncolours = numberOfColours;

	header.type = 'MB';

	header.offset = headerSize + infoheaderSize + sizeof(ColorRgba) * infoheader.ncolours;

	header.size = header.offset + imageSize;
}

void createBMPImage(Image *images, uint8_t *labels, uint32_t &numberOfImages) {
	HEADER header;

	INFOHEADER infoheader;

	createBMPheaders(header, infoheader, images);

	for (int i = 0; i < numberOfImages; i++) {
		std::stringstream name;

		name << "BMPS/" << (int)labels[i] << "_" << i << ".bmp";

		std::ofstream newBMP(name.str(), std::ios::binary);

		newBMP.write((char *)&header, headerSize);

		newBMP.write((char *)&infoheader, infoheaderSize);

		createColorPalette(newBMP);

		flipImageArray(images[i].pixels, images[0].rows, images[0].columns);

		assert((int)newBMP.tellp() == header.offset);

		newBMP.write((const char*)images[i].pixels, sizeof(images[i].pixels[0]) * images->rows * images->columns);

		assert((uint32_t)newBMP.tellp() == header.size);

		newBMP.close();
	}
	

	delete[] images;

	delete[] labels;
}

void createColorPalette(std::ofstream &file) {
	ColorRgba *colorTable = new ColorRgba[numberOfColours];

	for (int i = 0; i < numberOfColours; i++) {

		colorTable[i].blue = i;

		colorTable[i].green = i;

		colorTable[i].red = i;

		colorTable[i].reserved = 0;
	}
	file.write((const char *)colorTable, sizeof(ColorRgba) * numberOfColours);

	delete[] colorTable;
}