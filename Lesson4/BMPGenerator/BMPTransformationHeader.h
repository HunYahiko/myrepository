#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <cassert>

struct Image {

	uint8_t *pixels;
	uint32_t rows;
	uint32_t columns;
};

#pragma pack(push, 1)
struct HEADER {

	uint16_t type;
	uint32_t size;
	uint32_t reserved;
	uint32_t offset;

};
#pragma pack(pop)
const size_t headerSize = sizeof(HEADER);

struct INFOHEADER {

	unsigned int size;
	int width, height;
	unsigned short int planes;
	unsigned short int bits;
	unsigned int compression;
	unsigned int imagesize;
	int xresolution, yresolution;
	unsigned int ncolours;
	unsigned int importantcolours;

};
const size_t infoheaderSize = 40;

//const size_t rgbQuadSize;
struct ColorRgba {

	uint8_t blue;
	uint8_t green;
	uint8_t red;
	uint8_t reserved;

};

const size_t numberOfColours = 256;

const std::string imagesFileName = "t10k-images.idx3-ubyte";

const std::string labelsFileName = "t10k-labels.idx1-ubyte";

int smallEndianConverter(uint32_t number);

void flipImageArray(uint8_t *pixels, int numberOfRows, int numberOfColumns);

uint8_t * getLabels();

void checkIfFileIsOpen(std::ifstream &file, std::string fileName);

Image * getImages(uint32_t &numberOfImages);

void createBMPImage(Image *images, uint8_t *label, uint32_t &numberOfImages);

void createBMPheaders(HEADER &header, INFOHEADER &infoheader, Image *images);

void createColorPalette(std::ofstream &file);
