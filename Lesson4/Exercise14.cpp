#include <iostream>
#include <fstream>

const size_t numberOfColours = 256;
typedef struct image {
	/* KA
		- it's faster to store the pixels in continuous memory
		- you can "debug" the image using Image Watch
	*/
	unsigned char *pixels;
	uint32_t rows;
	uint32_t columns;
} Images;

const size_t headerSize = 14;
typedef struct {
	uint16_t type;
	uint32_t size;
	uint16_t reserved1, reserved2;
	uint32_t offset;
} HEADER;

const size_t infoheaderSize = 40;
typedef struct {
	unsigned int size;              
	int width, height;               
	unsigned short int planes;     
	unsigned short int bits;         
	unsigned int compression;        
	unsigned int imagesize;          
	int xresolution, yresolution;    
	unsigned int ncolours;          
	unsigned int importantcolours;  
} INFOHEADER;

//const size_t rgbQuadSize;
typedef struct {
	unsigned char blue;
	unsigned char green;
	unsigned char red;
	unsigned char reserved;
} RGBQuad;


void printImage(std::ostream &file, Images &image) {
	for(int i = 0; i < image.rows * image.columns; i++) {
			file << image.pixels[i];
		}
}

int smallEndianConverter(uint32_t number) {

	unsigned char first, second, third, fourth;

	first = number & 255;
	second = (number >> 8) & 255;
	third = (number >> 16) & 255;
	fourth = (number >> 24) & 255;


	return ((int)first << 24) + ((int)second << 16) + ((int)third << 8) + fourth;
}

int main() {

	std::ifstream testFile("t10k-images.idx3-ubyte", std::ios::binary);

	uint32_t magicNumber;
	uint32_t numberOfImages;
	uint32_t numberOfRows;
	uint32_t numberOfColumns;

	testFile.read((char*)&magicNumber, sizeof(uint32_t));
	magicNumber = smallEndianConverter(magicNumber);

	testFile.read((char*)&numberOfImages, sizeof(uint32_t));
	numberOfImages = smallEndianConverter(numberOfImages);

	testFile.read((char*)&numberOfRows, sizeof(uint32_t));
	numberOfRows = smallEndianConverter(numberOfRows);

	testFile.read((char*)&numberOfColumns, sizeof(uint32_t));
	numberOfColumns = smallEndianConverter(numberOfColumns);

	std::cout << numberOfRows << " " << numberOfColumns << std::endl;

	// KA: in C++ you don't do this ...
	// ... because it's automatically done when doing this
	Images firstImage;
	firstImage.pixels = new unsigned char[numberOfRows * numberOfColumns];

	firstImage.rows = numberOfRows;
	firstImage.columns = numberOfColumns;

	// KA: too much overhead for the file; it's faster to read big chunks in one shot ...
// 	}
	// ... like this:
	const size_t imageSize = sizeof(firstImage.pixels[0]) * numberOfRows * numberOfColumns;	// in bytes
	testFile.read((char*)firstImage.pixels, imageSize);

	std::cout << sizeof(HEADER) << " " << sizeof(INFOHEADER) << std::endl;
	HEADER header;
	memset(&header, 0, sizeof(header));	// results in setting all members to 0
	INFOHEADER infoHeader;
	memset(&infoHeader, 0, sizeof(infoHeader));	// results in setting all members to 0
	RGBQuad colorTable[numberOfColours];
	memset(&colorTable, 0, sizeof(RGBQuad));

	// KA: too much magic numbers; where possible, you should use the allready known values
	infoHeader.size = sizeof(INFOHEADER);
	infoHeader.width = numberOfColumns;
	infoHeader.height = numberOfRows;
	infoHeader.planes = 1;
	infoHeader.bits = 8;	// 8 bit grayscale image
	infoHeader.compression = 0;		// no compression
	infoHeader.imagesize = imageSize;	// optional when using no compression
	//infoHeader.xresolution = 0;
	//infoHeader.yresolution = 0;
	infoHeader.ncolours = numberOfColours;	// size of color palette; needed when bits <= 8; 256 colors (each shade of gray, including white and black) on 1 byte
	//infoHeader.importantcolours = 0;
	
	header.type = 'MB'; // 19778;
	//header.size = numberOfColumns * numberOfRows + sizeof(HEADER) - 2 + sizeof(INFOHEADER);
	//header.reserved1 = 0;
	//header.reserved2 = 0;
	// Info: "The header is 14 bytes in length." - from documentation
	// Question: How many bytes is sizeof(HEADER) ?
	// Tip: Where does the 14 byte come from? Try avoiding magic numbers again.
	// Tip: If you still want to use sizeof(HEADER), you need to "pragma pack" the HEADER structure
	header.offset = headerSize + infoheaderSize + 4 * infoHeader.ncolours;
	header.size = header.offset + imageSize;

	std::ofstream newBMP("firstBMP.bmp", std::ios::binary);
	newBMP.write((const char*)&header, headerSize);
	newBMP.write((const char*)&infoHeader, infoheaderSize);
	// TODO: write the color table here -> 256 * 4 bytes

	// SS: I try to write an indexed color table using a sructure of bytes and initialized it like this
	// But I am not sure if this is how you actually define a new color table
	// I found out that you create an array with rgb colors in which each index represents a color
	// A pixel from pixel array contains an index, which indicates to a color from color table.
	// But I am still not quite sure if this is how you add the color table.
	// I found an array which was already completed with rgb colors, but the image was showing only a blue color.
	for (int i = 0; i < numberOfColours; i++) {
		colorTable[i].blue = i;
		colorTable[i].green = i;
		colorTable[i].red = i;
	}
	for (int i = 0; i < numberOfColours; i++) {
		newBMP.write((const char*)&colorTable[i], sizeof(RGBQuad));
	}
	// TODO: write the image here
	// Note 1: you will need to flip the image vertically
	// Note 2: the size of one written line needs to be a multiple of 4; 
	// in the current case 28 % 4 = 0 so it's ok, but in other cases you would need to complete the line with extra padding bytes
	//unsigned int padding = (4 - ((3 * numberOfColumns) % 4)) % 4;
	
	for (int i = 0; i < numberOfRows * numberOfColumns; i++) {
		newBMP.write((char*)&firstImage.pixels[i], sizeof(unsigned char) * 4 );
	}
	//printImage(newBMP, firstImage);

	return 0;
}
