#pragma once
#include "Card.h"
#include <sstream>
#include <vector>
#include <climits>
#include "Bet.h"

class Player
{
public:
	const enum class Status
	{
		IDLE,
		WIN,
		LOSE
	};

	static const size_t scoreLimit = 21;
	Player();
	Player(size_t id, size_t score, std::string name);
	size_t getId();
	void setId(size_t id);
	size_t getScore();
	void setScore(size_t score);
	std::string getName();
	void setName(std::string name);
	Status getStatus();
	void setStatus(Status status);
	void addCard(const Card &card);
	std::vector<Card> getCards();
	Bet bet;
	bool isBroke();
	~Player();

private:
	static size_t playersNumber;
	std::string m_name;
	size_t m_id;
	size_t m_score;
	Status m_status;
	std::vector<Card> cards;

};

