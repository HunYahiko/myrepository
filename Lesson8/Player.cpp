#include "Player.h"

size_t Player::playersNumber = 0;

Player::Player() : Player(0, 0, "Player")
{

}

Player::Player(size_t id, size_t score, std::string name) :
	m_score(0) , m_name(name), bet(Bet())
{
	playersNumber++;
	m_id = playersNumber;
	m_status = Status::IDLE;
	
}


Player::~Player()
{
	
}

size_t Player::getId()
{
	return m_id;
}

void Player::setId(size_t id)
{
	m_id = id;
}

size_t Player::getScore()
{
	return m_score;
}

void Player::setScore(size_t score)
{
	m_score = score;
}

std::string Player::getName()
{
	return m_name;
}

void Player::setName(std::string name)
{
	m_name = name;
}

Player::Status Player::getStatus()
{
	return m_status;
}

void Player::setStatus(Player::Status status)
{
	m_status = status;
}

void Player::addCard(const Card &card)
{
	cards.push_back(card);
}

std::vector<Card> Player::getCards()
{
	return cards;
}

bool Player::isBroke()
{
	if (bet.getGeneralAmount() == 0)
	{
		return true;
	}
	
	return false;
}
