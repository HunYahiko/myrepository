#include "Card.h"
#include <iostream>
#include <string>

Card::Card(const Rank & rank, const Suit & suit) :
	m_rank(rank),
	m_suit(suit)
{
}

Card::Card(const Card & other) :
	Card(other.m_rank, other.m_suit)
{
}

Card::~Card()
{

}

Card & Card::operator=(const Card & other)
{
	m_rank = other.m_rank;
	m_suit = other.m_suit;

	return *this;
}


std::string Card::rankToString(const Rank& rank)
{
	int currentCardRank = static_cast<int>(rank);
	if (currentCardRank <= 10)
	{
		return std::to_string(currentCardRank);
	}

	switch (rank)
	{
	case Rank::JACK:
		return std::string("J");
	case Rank::QUEEN:
		return std::string("Q");
	case Rank::KING:
		return std::string("K");
	default:
		return std::string("A");
	}
}

std::ostream& operator << (std::ostream& o, const Card& target)
{
	o << target.rankToString(target.m_rank) << " " << static_cast<char>(target.m_suit);
	return o;
}

void Card::printCard() const
{
	std::cout << rankToString(m_rank) << " " << static_cast<char>(m_suit);
}

int Card::getCardValue() const
{
	int currentCardRank = static_cast<int>(m_rank);
	if (currentCardRank <= 10)
		return currentCardRank;
	if (currentCardRank <= 13)
		return 10;
	return 11;
}

bool Card::operator==(const Card &card)
{
	return m_rank == card.m_rank;
}


