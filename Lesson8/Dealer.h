#pragma once
#include <vector>
#include <sstream>
#include "Card.h"
#include "Player.h"
#include "Bet.h"
#include <climits>

class Dealer : public Player
{
private:
	std::string m_name;
	size_t m_score;
	Status m_status;
	std::vector<Card> cards;

public:
	static const size_t scoreLimit = 17;
	Bet bet;
	Dealer();
	Dealer(size_t score, std::string m_name);
	size_t getScore();
	void setScore(size_t score);
	std::string getName();
	void setName(std::string name);
	Status getStatus();
	void setStatus(Status status);
	void addCard(const Card &card);
	std::vector<Card> getCards();
	~Dealer();
};

