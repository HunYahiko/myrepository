#include "Bet.h"



Bet::Bet() : Bet(Bet::playerInitialAmount)
{

}

Bet::Bet(size_t generalAmount) : generalAmount(generalAmount)
{
	betAmount = 0;
}

Bet::~Bet()
{
}

size_t Bet::getBetAmount()
{
	return betAmount;
}

void Bet::addBetAmount(size_t amount)
{
	betAmount += amount;
}

size_t Bet::getGeneralAmount()
{
	return generalAmount;
}

void Bet::addGeneralAmount(size_t amount)
{
	generalAmount += amount;
}

void Bet::substractGeneralAmount(size_t amount)
{
	generalAmount -= amount;
}

void Bet::clearBets()
{
	betAmount = 0;
}
