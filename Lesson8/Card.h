#pragma once
#include <string>

class Card
{
public:
	enum class Rank
	{
		TWO = 2,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		TEN,
		JACK,
		QUEEN,
		KING,
		ACE
	};
	enum class Suit
	{
		HEARTS = 'H',
		DIAMONDS = 'D',
		CLUBS = 'C',
		SPADES = 'S'
	};

	static std::string rankToString(const Rank& rank);

	int getCardValue() const;

	Card(const Rank& rank = Rank::ACE, const Suit& suit = Suit::HEARTS);
	Card(const Card& other);
	~Card();
	Card& operator=(const Card& other);

	void printCard() const;
	friend std::ostream& operator << (std::ostream& o, const Card& target);
	bool operator==(const Card &card);

private:
	Rank m_rank;
	Suit m_suit;
};