#include "Deck.h"
#include "Card.h"

static const Card::Suit suits[4] =
{
	Card::Suit::HEARTS,
	Card::Suit::CLUBS,
	Card::Suit::DIAMONDS,
	Card::Suit::SPADES
};

size_t Deck::numberOfCardsInDeck = 0;

void Deck::printDeck() const
{
	for (const auto& card : m_array)
	{
 		card.printCard();
		std::cout << std::endl;
	}
}

Card Deck::dealCard()
{
	return m_array.at(m_index++);
}

Deck::Deck()
{
	std::cout << "How many decks do you want in the play? : ";
	std::cin >> numberOfCardsInDeck;

	if (numberOfCardsInDeck == 0)
	{
		numberOfCardsInDeck = oneDeckSize;
	}
	else if (numberOfCardsInDeck > 8)
	{
		numberOfCardsInDeck = 8;
		numberOfCardsInDeck *= oneDeckSize;
	}
	else
	{
		numberOfCardsInDeck *= oneDeckSize;
	}

	for (size_t j = 0; j < numberOfCardsInDeck / oneDeckSize; j++)
	{
		for (const auto suit : suits)
		{
			for (int i = static_cast<int>(Card::Rank::TWO); i <= static_cast<int>(Card::Rank::ACE); i++)
			{
				m_array.push_back(Card(static_cast<Card::Rank>(i), suit));
			}
		}
	}
}

Deck::~Deck()
{

}

void Deck::shuffleDeck()
{
	std::cout << m_array.size() << std::endl;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, numberOfCardsInDeck - 1);
	for (auto i = 0; i < numberOfCardsInDeck; ++i)
	{
		swap(m_array.at(i), m_array.at(dis(gen)));
	}
}

void Deck::swap(Card & firstCard, Card & secondCard)
{
	Card auxiliary = firstCard;
	firstCard = secondCard;
	secondCard = auxiliary;
}

size_t Deck::getNumberOfCards()
{
	return numberOfCardsInDeck;
}

bool Deck::isOverLimit()
{
	if (numberOfCardsInDeck > oneDeckSize)
	{
		if (numberOfCardsInDeck - m_index <= multipleDecksLimit)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else {
		if (numberOfCardsInDeck - m_index <= oneDeckLimit)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
