#include "Game.h"


const size_t initialCardsNumber = 2;
const size_t doubleAmount = 2;
const float oneAndHalfAmount = 1.5;
const size_t BlackJack = 21;

BlackJackGame::BlackJackGame() : m_dealer(Dealer())
{
	int number = 1;
	std::string name;

	std::cout << "How many players wish to play? : ";
	std::cin >> number;

	for (size_t i = 0; i < number; i++)
	{
		std::cout << "Enter player " << i + 1 << " name: ";
		if (i == 0)
		{
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}

		std::getline(std::cin, name);

		Player player(i, 0, name);

		m_players.push_back(player);

		std::cout << std::endl << std::endl;
	}

	m_deck.shuffleDeck();
	
	system("CLS");
}

BlackJackGame::~BlackJackGame()
{

}

char BlackJackGame::getPlayerChoice()
{
	char playerChoice;

	do {
		std::cout << "Press H to hit or S to stand: ";
		std::cin >> playerChoice;

	} while (playerChoice != 'H' && playerChoice != 'S');

	return playerChoice;
}


void BlackJackGame::play()
{
	if (m_deck.isOverLimit())
	{
		m_deck.shuffleDeck();
		std::cout << "ENTER SHUFFLE" << std::endl;
	}

	for (size_t i = 0; i < m_players.size(); i++)
	{
		if (m_players.at(i).isBroke())
		{
			std::cout << m_players.at(i).getName() << " has no more money and is out of game!" << std::endl;
			m_players.erase(m_players.begin() + i);
		}
	}

	for (auto &player : m_players)
	{
		placeBets(player);
	}

	setCards(m_dealer);

	std::cout << "Dealer cards: " << std::endl;
	std::cout << "[X] " << m_dealer.getCards().at(1) << std::endl << std::endl;
	
	for (auto &player : m_players)
	{
		setCards(player);
		showScore(player);
	}

	for (auto &player : m_players)
	{
		playRound(player);
	}

	playRound(m_dealer);

	determineWinner(m_players, m_dealer);

}

template <typename T>
void BlackJackGame::setCards(T &player)
{
	Card card;
	size_t cardScore = 0;

	for (size_t i = 0; i < initialCardsNumber; i++)
	{
		card = m_deck.dealCard();
		player.addCard(card);
		cardScore += card.getCardValue();
	}

	player.setScore(cardScore);
}


template <typename T>
void BlackJackGame::showScore(T &player) const
{
	std::cout << player.getName() << " cards: " << std::endl;

	for (auto &card : player.getCards())
	{
		card.printCard();
		std::cout << " | ";
	}

	std::cout << std::endl << player.getName() << " score: " << player.getScore()
		<< std::endl << std::endl;

}

template <typename T>
size_t BlackJackGame::hasAce(T &player) 
{
	Card aceCard;
	size_t count = 0;

	for (auto &card : player.getCards())
	{
		if (card == aceCard) {
			count++;
		}
	}

	return count;
}

template <typename T>
void BlackJackGame::playRound(T &player)
{
	bool isAceValueChanged = false;

	std::cout << player.getName() << " round: " << std::endl << std::endl;

	showScore(player);

	if (std::is_same<T, Dealer>::value)
	{
		while (player.getScore() < player.scoreLimit)
		{
			addScore(player, isAceValueChanged);

			if (player.getScore() > 21)
			{
				std::cout << player.getName() << " busted!" << std::endl << std::endl;

				player.setStatus(Dealer::Status::LOSE);
				player.bet.clearBets();

				isAceValueChanged = false;
				break;
			}
		}
	}
	else
	{
		while (getPlayerChoice() == 'H' && player.getScore() < player.scoreLimit)
		{
			addScore(player, isAceValueChanged);

			if (player.getScore() > 21)
			{
				std::cout << player.getName() << " busted!" << std::endl << std::endl;

				player.setStatus(Player::Status::LOSE);
				player.bet.clearBets();

				isAceValueChanged = false;
				break;
			}
		}
	}
}

template <typename T>
void BlackJackGame::addScore(T &player, bool &aceRule)
{
	
	Card card;
	size_t numberOfAces;

	card = m_deck.dealCard();

	player.addCard(card);

	auto cardValue = card.getCardValue();

	cardValue += player.getScore();

	player.setScore(cardValue);

	showScore(player);

	numberOfAces = hasAce(player);

	if (player.getScore() > player.scoreLimit && !aceRule && numberOfAces != 0)
	{
		std::cout << "Ace value changed to 1 due to bust" << std::endl << std::endl;

		aceRule = true;

		cardValue = player.getScore() - (10 * numberOfAces);

		player.setScore(cardValue);
	}

}

void BlackJackGame::determineWinner(std::vector<Player> &players, Dealer &dealer)
{
	std::cout << dealer.getName() << " score: " << dealer.getScore() << std::endl;

	if (dealer.getStatus() == Dealer::Status::LOSE)
	{
		for (auto &player : players)
		{
			std::cout << player.getName() << " score: " << player.getScore() << std::endl;

			if (player.getStatus() == Player::Status::LOSE)
			{
				std::cout << player.getName() << " loses!" << std::endl;
				player.bet.clearBets();
			}
			else
			{
				std::cout << player.getName() << " wins!" << std::endl;
				if (player.getCards().size() == initialCardsNumber && player.getScore() == BlackJack)
				{
					player.bet.addGeneralAmount(player.bet.getBetAmount() * oneAndHalfAmount);
					player.bet.clearBets();
				}
				else
				{
					player.bet.addGeneralAmount(player.bet.getBetAmount() * doubleAmount);
					player.bet.clearBets();
				}
			}
			std::cout << std::endl;
		}
	}
	else
	{
		for (auto &player : players)
		{
			std::cout << player.getName() << " score: " << player.getScore() << std::endl;

			if (player.getStatus() != Player::Status::LOSE)
			{
				if (isEqual(player, dealer))
				{
					std::cout << player.getName() << " push!" << std::endl;
					player.bet.addGeneralAmount(player.bet.getBetAmount());
					player.bet.clearBets();
				}
				else if (isBigger(player, dealer))
				{
					std::cout << player.getName() << " wins!" << std::endl;
					if (player.getCards().size() == initialCardsNumber && player.getScore() == BlackJack)
					{
						player.bet.addGeneralAmount(player.bet.getBetAmount() * oneAndHalfAmount);
						player.bet.clearBets();
					}
					else
					{
						player.bet.addGeneralAmount(player.bet.getBetAmount() * doubleAmount);
						player.bet.clearBets();
					}
				}
				else if (isLower(player, dealer))
				{
					std::cout << player.getName() << " loses" << std::endl;
					player.bet.clearBets();
				}
			}
		}
	}
}

void BlackJackGame::placeBets(Player &player)
{
	size_t betAmount;

	std::cout << player.getName() << " current bank: " << player.bet.getGeneralAmount() << " $." << std::endl;
	std::cout << player.getName() << " place your bet: " << std::endl;
	do
	{
		std::cin >> betAmount;
		if (betAmount > player.bet.getGeneralAmount())
		{
			std::cout << "You do not have such an amount to bet. Try again." << std::endl;
			std::cout << player.getName() << " place your bet: " << std::endl;
		}
		else if (betAmount == 0)
		{
			std::cout << "You must place an amount to play. Try again." << std::endl;
			std::cout << player.getName() << " place your bet: " << std::endl;
		}
	} while (betAmount > player.bet.getGeneralAmount() || betAmount == 0);

	player.bet.substractGeneralAmount(betAmount);
	size_t currentAmount = player.bet.getGeneralAmount();
	player.bet.addBetAmount(betAmount);
}

bool BlackJackGame::isEqual(Player &player, Dealer &dealer)
{
	return player.getScore() == dealer.getScore();
}

bool BlackJackGame::isBigger(Player &player, Dealer &dealer)
{
	return player.getScore() > dealer.getScore();
}

bool BlackJackGame::isLower(Player &player, Dealer &dealer)
{
	return player.getScore() < dealer.getScore();
}

