#pragma once
#include <iostream>
#include "Card.h"
#include <random>
#include <algorithm>

class Deck
{
	static size_t numberOfCardsInDeck;
	static const size_t oneDeckSize = 52;
	static const size_t oneDeckLimit = 22;
	static const size_t multipleDecksLimit = 60;
	std::vector<Card> m_array;
	size_t m_index = 0;

public:
	Deck();
	~Deck();
	void shuffleDeck();
	void swap(Card & firstCard,Card & secondCard);
	void printDeck() const;
	size_t getNumberOfCards();
	bool isOverLimit();
	Card dealCard();
};