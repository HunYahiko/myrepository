#include <iostream>
#include "Deck.h"
#include "Game.h"
int main()
{
	BlackJackGame game;
	char gameContinue = 'y';

	do
	{
		std::cout << std::endl << "---------------------------" << std::endl;
		game.play();

		std::cout << "Do you wish to continue?... " << std::endl;
		std::cout << "Yes(y) or No(n)" << std::endl;

		std::cin >> gameContinue;

	} while (gameContinue == 'y');

	return 0;






}
