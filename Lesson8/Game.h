#include <iostream>
#include "Deck.h"
#include "Player.h"
#include "Dealer.h"
#include <vector>


class BlackJackGame
{
public:

	BlackJackGame();
	~BlackJackGame();
	char getPlayerChoice();

	template <typename T>
	void setCards(T &player);

	void play();

	template <typename T>
	void showScore(T &player) const;

	template <typename T>
	size_t hasAce(T &player);

	template <typename T>
	void playRound(T &player);

	template <typename T>
	void addScore(T &player, bool &aceRule);

	void determineWinner(std::vector<Player> &players, Dealer &dealer);

	bool isBigger(Player &player, Dealer &dealer);
	bool isEqual(Player &player, Dealer &dealer);
	bool isLower(Player &player, Dealer &dealer);

	void placeBets(Player &player);

private:

	std::vector<Player> m_players;
	Dealer m_dealer;
	Deck m_deck;

};