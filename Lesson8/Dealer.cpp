#pragma once
#include "Dealer.h"
#include <iostream>

Dealer::Dealer() : Dealer(0, "Dealer")
{

}

Dealer::Dealer(size_t score, std::string name) :
	m_score(0), m_name("Dealer"), bet(Bet(UINT64_MAX))
{
	m_status = Status::IDLE;
}

Dealer::~Dealer()
{

}

size_t Dealer::getScore()
{
	return m_score;
}

void Dealer::setScore(size_t score)
{
	m_score = score;
}

std::string Dealer::getName()
{
	return m_name;
}

void Dealer::setName(std::string name)
{
	m_name = name;
}

Dealer::Status Dealer::getStatus()
{
	return m_status;
}

void Dealer::setStatus(Dealer::Status status)
{
	m_status = status;
}

void Dealer::addCard(const Card &card)
{
	cards.push_back(card);
}

std::vector<Card> Dealer::getCards()
{
	return cards;
}