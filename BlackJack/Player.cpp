#include "Player.h"
#include "Dealer.h"

size_t Player::playersNumber = 0;

Player::Player() : Player(0, 0, "Player", Bet())
{

}

Player::Player(size_t id, size_t score, std::string name, const Bet bet) :
	m_score(0) , m_name(name), bet(bet)
{
	playersNumber++;
	m_id = playersNumber;
	m_status = Status::IDLE;
	
}


Player::~Player()
{
	
}

size_t Player::getId() const
{
	return m_id;
}

void Player::setId(const size_t &id)
{
	m_id = id;
}

size_t Player::getScore() const
{
	return m_score;
}

void Player::setScore(const size_t &score)
{
	m_score = score;
}

std::string Player::getName() const
{
	return m_name;
}

void Player::setName(const std::string &name)
{
	m_name = name;
}

Player::Status Player::getStatus() const
{
	return m_status;
}

void Player::setStatus(const Player::Status &status)
{
	m_status = status;
}

void Player::addCard(const Card &card)
{
	cards.push_back(card);
}

std::vector<Card> Player::getCards() const
{
	return cards;
}

bool Player::isBroke()
{
	if (bet.getGeneralAmount() == 0)
	{
		return true;
	}
	
	return false;
}

bool Player::isEqual(Dealer &dealer)
{
	return this->getScore() == dealer.getScore();
}

bool Player::isBigger(Dealer &dealer)
{
	return this->getScore() > dealer.getScore();
}

bool Player::isLower(Dealer &dealer)
{
	return this->getScore() < dealer.getScore();
}

size_t Player::hasAce() const
{
	Card aceCard;
	size_t count = 0;

	for (auto const &card : this->getCards())
	{
		if (card == aceCard) {
			count++;
		}
	}

	return count;
}

void Player::clearHand()
{
	cards.clear();
	m_status = Player::Status::IDLE;
}

void Player::placeBets()
{
	size_t betAmount;

	std::cout << this->getName() << " current bank: " << this->bet.getGeneralAmount() << " $." << std::endl;
	std::cout << this->getName() << " place your bet: " << std::endl;
	do
	{
		std::cin >> betAmount;
		if (betAmount > this->bet.getGeneralAmount())
		{
			std::cout << "You do not have such an amount to bet. Try again." << std::endl;
			std::cout << this->getName() << " place your bet: " << std::endl;
		}
		else if (betAmount == 0)
		{
			std::cout << "You must place an amount to play. Try again." << std::endl;
			std::cout << this->getName() << " place your bet: " << std::endl;
		}
	} while (betAmount > this->bet.getGeneralAmount() || betAmount == 0);

	this->bet.substractGeneralAmount(betAmount);
	size_t currentAmount = this->bet.getGeneralAmount();
	this->bet.addBetAmount(betAmount);
}


void Player::showScore() const
{
	std::cout << this->getName() << " cards: " << std::endl;

	for (auto &card : this->getCards())
	{
		std::cout << card;
		std::cout << " | ";
	}

	std::cout << std::endl << this->getName() << " score: " << this->getScore()
		<< std::endl << std::endl;

}

void Player::playRound(Shoe &shoe)
{
	static bool isAceValueChanged = false;

	this->addScore(isAceValueChanged, shoe);

	if (this->getScore() > scoreLimit)
	{
		std::cout << this->getName() << " busted!" << std::endl << std::endl;

		this->setStatus(Player::Status::LOSE);
		this->bet.clearBets();

		isAceValueChanged = false;
	}
}

void Player::addScore(bool &aceRule, Shoe &shoe)
{
	Card card;
	size_t numberOfAces;

	card = shoe.dealCard();

	this->addCard(card);

	auto cardValue = card.getCardValue();

	cardValue += this->getScore();

	this->setScore(cardValue);

	this->showScore();

	numberOfAces = this->hasAce();

	if (this->getScore() > this->scoreLimit && !aceRule && numberOfAces != 0)
	{
		std::cout << "Ace value changed to 1 due to bust" << std::endl << std::endl;

		aceRule = true;

		cardValue = this->getScore() - (10 * numberOfAces);

		this->setScore(cardValue);
	}
}
