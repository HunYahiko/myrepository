#pragma once
#include <vector>
#include <sstream>
#include "Card.h"
#include "Player.h"
#include "Bet.h"
#include <climits>

class Dealer : public Player
{
public:
	static const size_t scoreLimit = 17;
	Dealer();
	~Dealer();

	void playRound(Shoe &shoe);

	void addScore(bool &aceRule, Shoe &shoe);
};

