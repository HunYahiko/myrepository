#include "Deck.h"
#include "Card.h"

static const Card::Suit suits[4] =
{
	Card::Suit::HEARTS,
	Card::Suit::CLUBS,
	Card::Suit::DIAMONDS,
	Card::Suit::SPADES
};

void Deck::printDeck() const
{
	for (const auto& card : m_array)
	{
		std::cout << card;
		std::cout << std::endl;
	}
}

Card Deck::dealCard()
{
	return m_array.at(m_index++);
}

Deck::Deck()
{
	for (const auto suit : suits)
	{
		for (int i = static_cast<int>(Card::Rank::TWO); i <= static_cast<int>(Card::Rank::ACE); i++)
		{
				m_array.push_back(Card(static_cast<Card::Rank>(i), suit));
		}
	}
}

Deck::~Deck()
{

}

void Deck::shuffleDeck()
{
	std::cout << m_array.size() << std::endl;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, deckSize - 1);
	for (auto i = 0; i < deckSize; ++i)
	{
		swap(m_array.at(i), m_array.at(dis(gen)));
	}
}

void Deck::swap(Card & firstCard, Card & secondCard)
{
	Card auxiliary = firstCard;
	firstCard = secondCard;
	secondCard = auxiliary;
}

size_t Deck::getNumberOfCards()
{
	return deckSize;
}

std::ostream& operator <<(std::ostream &out, const Deck &deck)
{
	for (auto &card : deck.m_array)
	{
		out << card << std::endl;
	}

	return out;
}

std::vector<Card> Deck::getCards() const
{
	return m_array;
}

bool Deck::hasNoCards()
{
	bool result;

	(m_index == deckSize - 1) ? result = true : result = false;

	return result;
}