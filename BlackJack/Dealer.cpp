#pragma once
#include "Dealer.h"
#include <iostream>

Dealer::Dealer() : Player(0, 0, "Dealer", Bet(UINT64_MAX))
{

}

Dealer::~Dealer()
{

}


void Dealer::playRound(Shoe &shoe)
{
	static bool isAceValueChanged = false;

	while (this->getScore() < scoreLimit)
	{
		this->addScore(isAceValueChanged, shoe);

		if (this->getScore() > Player::scoreLimit)
		{
			std::cout << this->getName() << " busted!" << std::endl << std::endl;

			this->setStatus(Player::Status::LOSE);
			this->bet.clearBets();

			isAceValueChanged = false;
			break;
		}
	}
}

void Dealer::addScore(bool &aceRule, Shoe &shoe)
{
	Card card;
	size_t numberOfAces;

	card = shoe.dealCard();

	this->addCard(card);

	auto cardValue = card.getCardValue();

	cardValue += this->getScore();

	this->setScore(cardValue);

	this->showScore();

	numberOfAces = this->hasAce();

	if (this->getScore() > this->scoreLimit && !aceRule && numberOfAces != 0)
	{
		std::cout << "Ace value changed to 1 due to bust" << std::endl << std::endl;

		aceRule = true;

		cardValue = this->getScore() - (10 * numberOfAces);

		this->setScore(cardValue);
	}
}