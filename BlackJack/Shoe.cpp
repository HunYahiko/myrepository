#include "Shoe.h"


Shoe::Shoe()
{
	size_t count = 0;
	
	std::cout << "Enter the number of decks: ";
	std::cin >> m_numberOfDecks;

	(m_numberOfDecks <= 0) ? m_numberOfDecks = oneDeck : m_numberOfDecks = m_numberOfDecks;

	(m_numberOfDecks > deckLimit) ? m_numberOfDecks = deckLimit : m_numberOfDecks = m_numberOfDecks;

	do
	{
		Deck deck;

		deck.shuffleDeck();

		m_decks.push_back(deck);

		count++;

	} while (count < m_numberOfDecks);
}

Shoe::~Shoe()
{
}

void Shoe::shuffleShoe()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0,  m_decks[0].getNumberOfCards() - 1);
	
	for (size_t i = 0; i < m_decks.size() - 1; i++)
	{
		for (size_t j = i + 1; j < m_decks.size() - 1; j++)
		{
			for (auto t = 0; t < m_decks[0].getNumberOfCards(); ++i)
			{
				swap(m_decks.at(i).getCards().at(t), m_decks.at(j).getCards().at(dis(gen)));
			}
		}
	}
}

Card Shoe::dealCard()
{
	static size_t deckCounter = 0;
	
	if (m_decks.at(deckCounter).hasNoCards())
	{
		deckCounter++;
	}

	return m_decks.at(deckCounter).getCards().at(m_currentIndex++);

}

void Shoe::swap(Card & firstCard, Card & secondCard)
{
	Card auxiliary = firstCard;
	firstCard = secondCard;
	secondCard = auxiliary;
}

bool Shoe::isOverLimit()
{
	bool result;

	size_t numberOfCards = m_decks.at(0).getNumberOfCards() * m_numberOfDecks;

	(numberOfCards - m_currentIndex <= cardLimit) ? result = true : result = false;

	return result;
}
