#pragma once

class Bet
{
private:
	size_t betAmount;
	size_t generalAmount;
	static const size_t playerInitialAmount = 100;

public:
	Bet();
	Bet(size_t generalAmount);
	size_t getBetAmount();
	void addBetAmount(size_t amount);
	size_t getGeneralAmount();
	void addGeneralAmount(size_t amount);
	void substractGeneralAmount(size_t amount);
	void clearBets();
	~Bet();
};

