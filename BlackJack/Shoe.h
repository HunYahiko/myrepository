#pragma once
#include "Deck.h"

class Shoe
{
private:
	static const size_t cardLimit = 60;
	static const size_t oneDeck = 1;
	static const size_t deckLimit = 8;
	std::vector<Deck> m_decks;
	size_t m_numberOfDecks;
	size_t m_currentIndex = 0;

public:
	Shoe();
	Card dealCard();
	void shuffleShoe();
	void swap(Card &firstCard, Card &secondCard);
	bool isOverLimit();
	~Shoe();
};

