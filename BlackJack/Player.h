#pragma once
#include "Card.h"
#include <vector>
#include <climits>
#include "Bet.h"
#include "Shoe.h"

class Dealer;

class Player
{
public:
	const enum class Status
	{
		IDLE,
		WIN,
		LOSE
	};

	static const size_t scoreLimit = 21;

	Player();
	Player(size_t id, size_t score, std::string name, const Bet bet);
	~Player();

	size_t getId() const;
	void setId(const size_t &id);

	size_t getScore() const;
	void setScore(const size_t &score);

	std::string getName() const;
	void setName(const std::string &name);

	Status getStatus() const;
	void setStatus(const Status &status);

	void addCard(const Card &card);
	std::vector<Card> getCards() const;

	Bet bet;

	bool isBroke();

	bool isEqual(Dealer &dealer);
	bool isBigger(Dealer &dealer);
	bool isLower(Dealer &dealer);

	size_t hasAce() const;

	void clearHand();

	void placeBets();

	void showScore() const;

	void playRound(Shoe &shoe);

	void addScore(bool &aceRule, Shoe &shoe);

private:
	static size_t playersNumber;
	std::string m_name;
	size_t m_id;
	size_t m_score;
	Status m_status;
	std::vector<Card> cards;
};

