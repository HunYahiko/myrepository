#include <iostream>
#include "Shoe.h"
#include "Player.h"
#include "Dealer.h"
#include <vector>


class BlackJackGame
{
public:

	BlackJackGame();
	~BlackJackGame();
	char getPlayerChoice();

	template <typename T>
	void setCards(T &player);

	void play();

	void determineWinner(std::vector<Player> &players, Dealer &dealer);


private:

	std::vector<Player> m_players;
	Dealer m_dealer;
	Shoe m_shoe;

};