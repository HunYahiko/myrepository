#include "Game.h"


const size_t initialCardsNumber = 2;
const size_t doubleAmount = 2;
const float oneAndHalfAmount = 1.5;
const size_t BlackJack = 21;

BlackJackGame::BlackJackGame() : m_dealer(Dealer()), m_shoe(Shoe())
{
	int number = 1;
	std::string name;

	std::cout << "How many players wish to play? : ";
	std::cin >> number;

	for (size_t i = 0; i < number; i++)
	{
		std::cout << "Enter player " << i + 1 << " name: ";
		if (i == 0)
		{
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}

		std::getline(std::cin, name);

		Player player(i, 0, name, Bet());

		m_players.push_back(player);

		std::cout << std::endl << std::endl;
	}

	m_shoe.shuffleShoe();
	
	system("CLS");
}

BlackJackGame::~BlackJackGame()
{

}

char BlackJackGame::getPlayerChoice()
{
	char playerChoice;

	do {
		std::cout << "Press H to hit or S to stand: ";
		std::cin >> playerChoice;

	} while (playerChoice != 'H' && playerChoice != 'S');

	return playerChoice;
}


void BlackJackGame::play()
{
	char choice = 'S';

	if (m_shoe.isOverLimit())
	{
		m_shoe.shuffleShoe();
	}

	for (size_t i = 0; i < m_players.size(); i++)
	{
		if (m_players.at(i).isBroke())
		{
			std::cout << m_players.at(i).getName() << " has no more money and is out of game!" << std::endl;
			m_players.erase(m_players.begin() + i);
		}
	}

	for (auto &player : m_players)
	{
		player.clearHand();
		player.placeBets();
	}

	m_dealer.clearHand();
	setCards(m_dealer);

	std::cout << "Dealer cards: " << std::endl;
	std::cout << "[X] " << m_dealer.getCards().at(1) << std::endl << std::endl;
	
	for (auto &player : m_players)
	{
		setCards(player);
		player.showScore();
	}

	for (auto &player : m_players)
	{
		std::cout << player.getName() << " round: " << std::endl << std::endl;

		player.showScore();

		while (player.getScore() < player.scoreLimit)
		{
			choice = getPlayerChoice();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

			if (choice == 'S')
			{
				break;
			}

			player.playRound(m_shoe);
		}
	}

	std::cout << m_dealer.getName() << " round:" << std::endl << std::endl;
	m_dealer.showScore();

	m_dealer.playRound(m_shoe);

	determineWinner(m_players, m_dealer);

}

template <typename T>
void BlackJackGame::setCards(T &player)
{
	Card card;
	size_t cardScore = 0;
	size_t count = 0;

	do
	{
		card = m_shoe.dealCard();
		player.addCard(card);
		cardScore += card.getCardValue();
		count++;
	} while (count < 2);

	player.setScore(cardScore);
}


void BlackJackGame::determineWinner(std::vector<Player> &players, Dealer &dealer)
{
	std::cout << dealer.getName() << " score: " << dealer.getScore() << std::endl;

	if (dealer.getStatus() == Dealer::Status::LOSE)
	{
		for (auto &player : players)
		{
			std::cout << player.getName() << " score: " << player.getScore() << std::endl;

			if (player.getStatus() == Player::Status::LOSE)
			{
				std::cout << player.getName() << " loses!" << std::endl;
				player.bet.clearBets();
			}
			else
			{
				std::cout << player.getName() << " wins!" << std::endl;
				if (player.getCards().size() == initialCardsNumber && player.getScore() == BlackJack)
				{
					player.bet.addGeneralAmount(player.bet.getBetAmount() * oneAndHalfAmount);
					player.bet.clearBets();
				}
				else
				{
					player.bet.addGeneralAmount(player.bet.getBetAmount() * doubleAmount);
					player.bet.clearBets();
				}
			}
			std::cout << std::endl;
		}
	}
	else
	{
		for (auto &player : players)
		{
			std::cout << player.getName() << " score: " << player.getScore() << std::endl;

			if (player.getStatus() != Player::Status::LOSE)
			{
				if (player.isEqual(dealer))
				{
					std::cout << player.getName() << " push!" << std::endl;
					player.bet.addGeneralAmount(player.bet.getBetAmount());
					player.bet.clearBets();
				}
				else if (player.isBigger(dealer))
				{
					std::cout << player.getName() << " wins!" << std::endl;
					if (player.getCards().size() == initialCardsNumber && player.getScore() == BlackJack)
					{
						player.bet.addGeneralAmount(player.bet.getBetAmount() * oneAndHalfAmount);
						player.bet.clearBets();
					}
					else
					{
						player.bet.addGeneralAmount(player.bet.getBetAmount() * doubleAmount);
						player.bet.clearBets();
					}
				}
				else if (player.isLower(dealer))
				{
					std::cout << player.getName() << " loses!" << std::endl;
					player.bet.clearBets();
				}
			}
		}
	}
}




