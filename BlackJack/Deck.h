#pragma once
#include <iostream>
#include "Card.h"
#include <random>
#include <algorithm>

class Deck
{
	static const size_t deckSize = 52;
	std::vector<Card> m_array;
	size_t m_index = 0;

public:
	Deck();
	~Deck();
	void shuffleDeck();
	void swap(Card & firstCard,Card & secondCard);
	void printDeck() const;
	size_t getNumberOfCards();
	Card dealCard();
	friend std::ostream& operator <<(std::ostream &out, const Deck &deck);
	std::vector<Card> getCards() const;
	bool hasNoCards();
};