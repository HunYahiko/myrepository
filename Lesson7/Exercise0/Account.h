#pragma once
#include <sstream>
#include <fstream>

class Account
{
private:
	double myDeposit;
	int id;
	static int currentId;
	std::ofstream myFile;

public:
	Account();
	Account(double value);
	~Account();
	void widthdraw(const double& amount);
	void deposit(const double &amount);
	double balance();
	void recordTransaction(std::string transactionType, const double &amount);
	int getId();
	void setId(int newId);
};

