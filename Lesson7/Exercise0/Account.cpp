#include "Account.h"

int Account::currentId = 0;

Account::Account()
{
	myDeposit = 0;
	currentId++;
	id = currentId;

	std::stringstream name;
	name << "transactions" << id << ".txt";

	myFile.open(name.str(), std::ios::app);
}


Account::~Account()
{
	myFile.close();
}

Account::Account(double value) : myDeposit(value) 
{
	currentId++;
	id = currentId;

	std::stringstream name;
	name << "transactions" << id << ".txt";

	myFile.open(name.str(), std::ios::app);
}


void Account::widthdraw(const double &amount) {
	myDeposit -= amount;
	this -> recordTransaction("widthdraw", amount);
}

void Account::deposit(const double &amount) {
	myDeposit += amount;
	this -> recordTransaction("deposit", amount);
}

double Account::balance() {
	return myDeposit;
}

int Account::getId() {
	return id;
}

void Account::setId(int newId) {
	id = newId;
}

void Account::recordTransaction(std::string transactionType, const double &amount)
{
	
	std::stringstream input;

	input << transactionType << ": " << amount << " lei" << std::endl;

	myFile << input.str();

}
