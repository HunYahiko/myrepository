#include <iostream>
#include <sstream>
#include <vector>

struct Student
{
	char* name;
	int age;
	char* specialization;
};

const int maxName = 30;
const int maxSpecialization = 40;

void getNumberOfStudents(int &numberOfStudents)
{
	std::cout << "Enter number of students you want to write in: ";
	std::cin >> numberOfStudents;
}

void getStudents(Student *students, int &numberOfStudents)
{
	for (size_t i = 0; i < numberOfStudents; i++)
	{
		students[i].name = new char[maxName];

		students[i].specialization = new char[maxSpecialization];

		std::cout << "Give student " << i << " a name: ";
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin.get(students[i].name, maxName);
		
		std::cout << "Give student " << i << " an age: ";
		std::cin >> students[i].age;

		std::cout << "Give student " << i << " a specialization: ";
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cin.get(students[i].specialization, maxSpecialization);

		std::cout << std::endl;

	}
}

bool compareNames(Student *students, int &index)
{
	switch (strcmp(students[index].name, students[index + 1].name)) {
	case -1:
		std::swap(students[index], students[index + 1]);

		return true;

		break;

	case 1:
		return true;

		break;

	case 0:
		return false;

		break;
	}
}

void compareSpecialization(Student *students, int &index)
{
	if (strcmp(students[index].specialization, students[index + 1].specialization) >= 1)
	{
		std::swap(students[index], students[index + 1]);
	}
}

void bubbleSort(Student *students, int numberOfStudents) {
	
	do {
		for (int i = 0; i < numberOfStudents - 1; i++) 
		{
			if (students[i].age > students[i + 1].age) 
			{
				std::swap(students[i], students[i + 1]);
			} 
			else if (students[i].age == students[i + 1].age)
			{
				if (!compareNames(students, i)) 
				{
					compareSpecialization(students, i);
				}
			}
		}
		numberOfStudents--;
	} while (numberOfStudents > 1);
}

int main() {

	Student *students;
	int numberOfStudents;

	getNumberOfStudents(numberOfStudents);

	students = new Student[numberOfStudents];

	getStudents(students, numberOfStudents);

	bubbleSort(students, numberOfStudents);

	for (size_t i = 0; i < numberOfStudents; i++) {
		std::cout << students[i].name << " " << students[i].age << " " << students[i].specialization << std::endl;
	}

	return 0;

}