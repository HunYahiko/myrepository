#include "Student.h"

std::vector<Student> Student::students;


Student::Student()
{
	name = "null";
	age = 0;
	specialization = "null";
	
	students.push_back(*this);
}

Student::Student(std::string name, int age, std::string specialization) : name(name), age(age), specialization(specialization)
{
	students.push_back(*this);
}

Student::Student(std::string name) : name(name)
{
	age = 0;
	specialization = "null";
}

Student::Student(std::ifstream &file)
{
	if (!file.is_open())
	{
		std::cout << "Error opening file!" << std::endl;
		exit(0);
	}

	while (!file.eof())
	{
		file >> name >> age >> specialization;
		students.push_back(*this);
	}
}


Student::~Student()
{
}

void Student::addStudent()
{
	std::string name;
	int age;
    std::string specialization;

	std::cout << "Enter new student name: ";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(std::cin, name);

	std::cout << "Enter new student age: ";
	std::cin >> age;

	std::cout << "Enter new student specialization: ";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(std::cin, specialization);

	std::cout << std::endl;

	if (!isDuplicate(name, age, specialization))
	{
		Student newStudent(name, age, specialization);
		std::cout << "New student has been added" << std::endl;
	}
	else
	{
		std::cout << "This student already exists!" << std::endl;
	}
}

void Student::deleteStudent(std::string name)
{
	bool deleted = false;
	for (size_t i = 0; i < students.size(); i++)
	{
		if (students.at(i).name == name)
		{
			students.erase(students.begin() + i);
			deleted = true;
		}
	}
	if (!deleted)
	{
		std::cout << "Student with such name was not found!" << std::endl;
		return;
	}

	std::cout << "Student has been deleted" << std::endl;
}

bool Student::compare(std::string newName, int newAge, std::string newSpecialization)
{
	return name == newName && age == newAge && specialization == newSpecialization;
}

bool Student::isDuplicate(std::string name, int age, std::string specialization)
{
	for (size_t i = 0; i < students.size(); i++)
	{
		if (students.at(i).compare(name, age, specialization))
		{
			std::cout << "This student already exists" << std::endl;
			return true;
		}
	}
	
	return false;
}

void Student::showStudents() 
{
	for (size_t i = 0; i < students.size(); i++)
	{
		std::cout << students.at(i).name << " " << students.at(i).age << " " << students.at(i).specialization << std::endl;
	}
}

bool Student::compareNames(size_t &index)
{
	if (students.at(index).name > students.at(index + 1).name)
	{
		std::swap(students.at(index), students.at(index + 1));

		return true;
	}
	else if (students.at(index).name == students.at(index + 1).name)
	{
		return false;
	}
	else
	{
		return false;
	}
}

void Student::compareSpecializations(size_t &index)
{
	if (students.at(index).specialization > students.at(index + 1).specialization)
	{
		std::swap(students.at(index), students.at(index + 1));
	}
}

void Student::sort()
{
	size_t vectorSize = students.size();
	do
	{
		for (size_t i = 0; i < vectorSize - 1; i++)
		{
			if (students.at(i).age > students.at(i + 1).age)
			{
				std::swap(students.at(i), students.at(i + 1));
			}
			else if (students.at(i).age == students.at(i + 1).age)
			{
				if (!this->compareNames(i))
				{
					this->compareSpecializations(i);
				}
			}
		}
		vectorSize--;
	} while (vectorSize > 1);
	
	std::cout << "The list of students has been sorted" << std::endl;
}