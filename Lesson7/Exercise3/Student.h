#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

class Student
{
private:
	std::string name;
	int age;
	std::string specialization;
	static std::vector<Student> students;
	
public:
	Student();
	Student(std::string name);
	Student(std::string name, int age, std::string specialization);
	Student(std::ifstream &file);
	void addStudent();
	void deleteStudent(std::string name);
	bool isDuplicate(std::string name, int age, std::string specialization);
	void showStudents();
	bool compareNames(size_t &index);
	void compareSpecializations(size_t &index);
	void sort();
	bool compare(std::string newName, int newAge, std::string newSpecialization);
	~Student();
};

