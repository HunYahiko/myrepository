#include <iostream>
#include "Student.h"

void executeAction(int &action, Student &manager, bool &endLoop)
{
	system("CLS");
	std::string name;
	switch (action)
	{
	case 1:
		manager.showStudents();

		std::cout << "Press any button to continue..." << std::endl;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		getchar();

		break;

	case 2:
		manager.addStudent();

		std::cout << "Press any button to continue..." << std::endl;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		getchar();

		break;

	case 3:
		std::cout << "Give the name of student you want to delete: ";
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		std::getline(std::cin, name);

		manager.deleteStudent(name);

		std::cout << "Press any button to continue..." << std::endl;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		getchar();

		break;

	case 4:
		manager.sort();

		std::cout << "Press any button to continue..." << std::endl;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		getchar();

		break;

	case 5:
		endLoop = false;

		std::cout << "Goodbye! Press any button to continue..." << std::endl;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		break;

	default:
		std::cout << "An action with such index does not exist! Try again.";
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		getchar();

		break;
	}
}

int main() {

	std::ifstream studentFile("students.txt");

	Student manager("manager");
	Student file(studentFile);

	bool start = true;
	int action = 0;

	std::cout << "Students from file were introduced into system" << std::endl;
	std::cout << "Press any button to continue..." << std::endl;

	getchar();

	while (start)
	{
		system("CLS");
		std::cout << "Student's GUI" << std::endl;
		std::cout << "1.Show current students" << std::endl;
		std::cout << "2.Add student" << std::endl;
		std::cout << "3.Delete student" << std::endl;
		std::cout << "4.Sort student's list" << std::endl;
		std::cout << "5.Exit" << std::endl;
		std::cout << "Choose your action: ";

		std::cin >> action;

		executeAction(action, manager, start);
	}

	return 0;
}