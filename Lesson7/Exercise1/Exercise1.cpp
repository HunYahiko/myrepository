#include <iostream>

bool isPalindrome(char *word);

const int MAXSIZE = 20;

int main() 
{
	char *word = new char[MAXSIZE];

	std::cout << "Enter the word: ";
	std::cin >> word;

	if (isPalindrome(word))
	{
		std::cout << "Your word is a palindrome." << std::endl;
	}
	else
	{
		std::cout << "Your word is not a palindrome" << std::endl;
	}

	return 0;

}

bool isPalindrome(char *word)
{
	size_t wordSize = std::strlen(word) - 1;
	size_t *index1 = 0;
	size_t *index2 = &wordSize;

	while (*index2 > *index1) 
	{
		if (*(word + *index1) != *(word + *index2))
		{
			return false;
		}
		*index1++;
		*index2--;
	}
	return true;

}