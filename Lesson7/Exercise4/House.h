#pragma once
#include "Address.h"
#include "Room.h"
#include <vector>

class House
{
private:
	Address address;
	std::vector<Room> rooms;
	std::string houseName;

public:
	House();
	House(std::string houseName, Address address, std::vector<Room> &rooms );
	void showRooms();
	void setRooms(std::vector<Room> &newRooms);
	void showAddress();
	void setAddress(Address &newAddress);
	void showHouse();
	std::string getHouseName();
	void setHouseName(std::string newHouseName);
	friend std::ostream& operator<<(std::ostream &out, const House &house);
	~House();
};

