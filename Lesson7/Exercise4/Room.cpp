#include "Room.h"



Room::Room()
{
	length = 0;
	breadth = 0;
	height = 0;
}

Room::Room(double length, double breadth, double height) : length(length), breadth(breadth), height(height)
{

}

Room::~Room()
{

}

double Room::getLength() { return length; }

void Room::setLength(double newLength) { length = newLength; }

double Room::getBreadth() { return breadth; }

void Room::setBreadth(double newBreadth) { breadth = newBreadth; }

double Room::getHeight() { return height; }

void Room::setHeight(double newHeight) { height = newHeight; }



std::ostream& operator<<(std::ostream &out, const Room &room)
{
	out << "length: "<< room.length << "m " << "breadth: " 
		<< room.breadth << "m " << "height: " << room.height << "m";
	return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<Room> &rooms)
{
	for (size_t i = 0; i < rooms.size(); i++)
	{
		out << "Room number " << i + 1 << " : " << rooms.at(i) << std::endl;
	}
	
	return out;
}

