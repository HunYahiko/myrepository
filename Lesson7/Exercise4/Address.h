#pragma once
#include <iostream>
#include <sstream>


class Address
{
private:
	std::string address;
	std::string city;
	std::string state;
	int houseNumber;
public:
	Address();
	Address(std::string address, std::string city, std::string state, int houseNumber);
	std::string getAddress();
	void setAddress(std::string newAddress);
	std::string getCity();
	void setCity(std::string newCity);
	std::string getState();
	void setState(std::string newState);
	int getHouseNumber();
	void setHouseNumber(int newHouseNumber);
	friend std::ostream& operator<<(std::ostream &out, const Address &address);
	~Address();
};

