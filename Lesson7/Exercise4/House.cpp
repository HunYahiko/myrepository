#include "House.h"



House::House()
{
	houseName = "null";
	address;
	rooms;
}

House::House(std::string houseName, Address address, std::vector<Room> &rooms)
	: houseName(houseName) , address(address) , rooms(rooms)
{

}



std::string House::getHouseName() { return houseName; }

void House::setHouseName(std::string newHouseName) { houseName = newHouseName; }



void House::showRooms()
{
	for (size_t i = 0; i < rooms.size(); i++) 
	{
		std::cout << "Room number " << i << " : " << rooms.at(i) << std::endl;
	}
}

void House::setRooms(std::vector<Room> &newRooms)
{
	rooms = newRooms;
}



void House::showAddress()
{
	std::cout << "Address: " << address << std::endl;
}

void House::setAddress(Address &newAddress)
{
	address = newAddress;
}


void House::showHouse()
{
	std::cout << "House name: " << houseName << std::endl;
	showAddress();
	showRooms();
}


std::ostream& operator<<(std::ostream &out, const House &house)
{
	out << "House:" << std::endl
		<< "House name: " << house.houseName << std::endl << std::endl
		<< house.address << std::endl << house.rooms;

	return out;
}


House::~House()
{
}
