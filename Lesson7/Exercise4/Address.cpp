#include "Address.h"


Address::Address()
{
	address = "null";
	city = "null";
	state = "null";
	houseNumber = 0;
}

Address::Address(std::string address, std::string city, std::string state, int houseNumber)
	: address(address), city(city), state(state), houseNumber(houseNumber)
{

}

std::string Address::getAddress() { return address; }

void Address::setAddress(std::string newAddress) { address = newAddress; }

std::string Address::getCity() { return city; }

void Address::setCity(std::string newCity) { city = newCity; }

std::string Address::getState() { return state; }

void Address::setState(std::string newState) { state = newState; }

int Address::getHouseNumber() { return houseNumber; }

void Address::setHouseNumber(int newHouseNumber) { houseNumber = newHouseNumber; }

std::ostream& operator<<(std::ostream &out, const Address &address)
{
	out << "Address: " << address.address << std::endl << "City: " << address.city
		<< std::endl << "State: " << address.state << std::endl << "House number: " << address.houseNumber << std::endl;
	return out;
}


Address::~Address()
{
}
