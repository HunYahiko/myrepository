#include "House.h"

void setRooms(size_t &numberOfRooms, std::vector<Room> &rooms)
{
	for (size_t i = 0; i < numberOfRooms; i++)
	{
		double length, breadth, height;

		std::cout << "Enter room number " << i + 1 << " length: ";
		std::cin >> length;

		std::cout << "Enter room number " << i + 1 << " breadth: ";
		std::cin >> breadth;

		std::cout << "Enter room number " << i + 1 << " height: ";
		std::cin >> height;

		Room room(length, breadth, height);

		rooms.push_back(room);
	}
}

void setAddress(Address &houseAddress)
{
	std::string address;
	std::string city;
	std::string state;
	int houseNumber;

	std::cout << "Enter the address: ";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(std::cin, address);

	std::cout << "Enter the city: ";
	//std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(std::cin, city);

	std::cout << "Enter the state: ";
	//std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(std::cin, state);

	std::cout << "Enter the house number: ";
	std::cin >> houseNumber;

	houseAddress.setAddress(address);

	houseAddress.setCity(city);

	houseAddress.setState(state);

	houseAddress.setHouseNumber(houseNumber);
}

void createHouse(House &house, Address &address, std::vector<Room> &rooms)
{
	std::string houseName;

	std::cout << "Enter the house name: ";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(std::cin, houseName);

	house.setHouseName(houseName);
	
	house.setAddress(address);

	house.setRooms(rooms);
}


int main() {
	
	size_t numberOfRooms;
	std::vector<Room> rooms;
	Address address;
	House house;

	std::cout << "Enter the number of rooms in the house: ";
	std::cin >> numberOfRooms;

	setRooms(numberOfRooms, rooms);

	setAddress(address);

	createHouse(house, address, rooms);

	system("CLS");
	std::cout << house << std::endl;

	return 0;

}