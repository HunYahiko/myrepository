#pragma once
#include <iostream>
#include <vector>

class Room
{
private:
	double length;
	double breadth;
	double height;

public:
	Room();
	Room(double length, double breadth, double height);
	double getLength();
	void setLength(double newLength);
	double getBreadth();
	void setBreadth(double newBreadth);
	double getHeight();
	void setHeight(double newHeight);
	friend std::ostream& operator<<(std::ostream &out, const Room &room);
	friend std::ostream& operator<<(std::ostream &out, const std::vector<Room> &rooms);
	~Room();
};



